#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#define PORT 8080
#define MAX 1024

//global
char buffer[MAX] = {0};
char user[MAX] = {0};
char absPath[300] = "/home/yunus/Praktikum/modul3/Server";
int new_socket;
char *msg;

//global file
FILE* f_akun;
FILE* f_problems;
char akunPath[300];
char problemsPath[300];
char dirPath[300];

void* kirim2(void* arg){
    char* value = (char*)arg;
    send(new_socket , value , strlen(value) , 0);
}

void kirim(char string[]){
    pthread_t thread1;
    pthread_create(&thread1, NULL, &kirim2, (void* )string);
    pthread_join(thread1, NULL);
}

void* terima2(){
    read( new_socket , buffer, MAX);
}

void terima(){
    pthread_t thread1;
    pthread_create(&thread1, NULL, &terima2, NULL);
    pthread_join(thread1, NULL);
}

//register
void regist(){
    char akun[MAX];   
        f_akun = fopen(akunPath, "a+");
        //get username
        char buf[MAX];
        char tempString[MAX];
        while((fgets(buf, MAX, f_akun)!=NULL)) {  //good to handle error as well
            strcat(tempString, buf);
            strcat(tempString, "\n");
        }

        msg = "Please fill this registraton form: \nUsername: ";
        kirim(msg);
        while (1)
        {            
            bzero(buffer, MAX);
            terima();
            printf("%s\n",buffer ); 
            if(strstr(tempString, buffer)==NULL) {
                msg = "OK";
                kirim(msg);
                break;
            }else{
                msg = "Username is exist, please try different username";
                kirim(msg);
            }
            /* code */
        }
        
        //keep username
        strcpy(akun, buffer);
        strcat(akun, ":");

        //get pass
        msg = "\nPassword: ";
        kirim(msg);

        bzero(buffer, MAX);
        terima();
        printf("%s\n",buffer);
        
        //keep pass
        strcat(akun, buffer);
        strcat(akun, "\n");

        fputs(akun, f_akun);
        fclose(f_akun);
}


//login
int login(){
        char akun[MAX];
        f_akun = fopen(akunPath, "r");
        int success = 0;

        msg = "Login\n";
        kirim(msg);

        char buf[MAX];
        char tempString[MAX];
        while((fgets(buf, MAX, f_akun)!=NULL)) {  //good to handle error as well
            strcat(tempString, buf);
            strcat(tempString, "\n");
        }
       while (success == 0)
       {    
            msg = "username login: ";
            kirim(msg);
            
            bzero(buffer, MAX);
            terima();
            printf("%s\n",buffer );

            //save the curr-username
            strcpy(user, buffer);

            //keep username
            strcpy(akun, buffer);
            strcat(akun, ":");

            //get pass
            msg = "\nPassword: ";
            kirim(msg);

            bzero(buffer, MAX);
            terima();
            printf("%s\n",buffer);
            
            //keep pass
            strcat(akun, buffer);

            if(strstr(tempString, akun)!=NULL) {
                success = 1;
            }
            printf("%d\n",success);
            if(success == 0){
                msg = "!! wrong username/password\n";
                kirim(msg);
            }
            else{
                msg = "login success\n";
                kirim(msg);
            }    
            bzero(akun, MAX);
        }
        fclose(f_akun); 
    return success;
}

void write_file(char fileName[])
{
    int n; 
    FILE *fp;
    char *filename = fileName;

    char filePath[MAX];
    strcpy(filePath, dirPath);
    strcat(filePath, "/");
    strcat(filePath, fileName);

    fp = fopen(filePath, "a+");
    if(fp==NULL)
    {
        perror("[-]Error in creating file.");
        exit(1);
    }
    bzero(buffer, MAX);
    terima();
    fprintf(fp, "%s", buffer);
    printf("%s",buffer);
    bzero(buffer, MAX);
    fclose(fp);
    return;
    
}


int main(int argc, char const *argv[]) {
    int server_fd;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);

    int isAuth = 0;

    //make user.txt file
    strcpy(akunPath, absPath);
    strcat(akunPath, "/users.txt");
    f_akun = fopen(akunPath, "a+");
    fclose(f_akun);
    //makeprblem.tsv
    strcpy(problemsPath, absPath);
    strcat(problemsPath, "/problems.tsv");
    f_problems = fopen(problemsPath, "a+");
    fclose(f_problems);
    //
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    //login / signin
    msg = "Enter 0 for Register \nEnter 1 for login ";
    kirim(msg);
    
    bzero(buffer, MAX);
    terima();
    printf("%s\n",buffer );
    
    if(strcmp(buffer, "0") == 0){
        regist();
        isAuth = login();
    }else if (strcmp(buffer, "1") == 0){ //login
        isAuth = login();
    }
    bzero(buffer, MAX);


    //if login
    while (isAuth)
    {
    f_problems = fopen(problemsPath, "a+");
        msg = "welcome, anda telah login\nketik 'add' untuk menambahkan problem/soal baru\nketik 'see' untuk melihat list problem\nketik 'download <judul-problem>' untuk mendownload problem\nketik 'submit <judul-problem> <path-output.txt>' untuk submit jawaban" ;
        kirim(msg);

        terima();
        printf("%s\n",buffer);


        char listProblem[MAX];
        char buf[MAX];
        while((fgets(buf, MAX, f_problems)!=NULL)) {  //good to handle error as well
            strcat(listProblem, buf);
        }

        if(strcmp(buffer, "add") == 0){
            char saveData[MAX];
            //check judul if exist
            while (1)
            {
                msg = "Judul Problem";
                kirim(msg);
                                
                bzero(buffer, MAX);
                terima();
                printf("%s\n",buffer );

                if(strstr(listProblem, buffer)==NULL) {
                    msg = "OK";
                    kirim(msg);
                    break;
               }else{
                    msg = "Judul telah ada, silahkan gunakn judul lain\n";
                    kirim(msg);                    
                }
            }
            //here buffer = judul
            mkdir(buffer, 0777);
            strcpy(dirPath, absPath);
            strcat(dirPath, "/");
            strcat(dirPath, buffer);
            //update database problems.tsv
            strcpy(saveData, buffer);
            strcat(saveData, "\t");
            strcat(saveData, user);


            fprintf(f_problems, "%s\n", saveData);

            char pathDesc[MAX];
            char pathIn[MAX];
            char pathOut[MAX];

            msg = "Filepath description.txt:";
            kirim(msg);
            //
            bzero(buffer, MAX);
            terima();
            printf("%s\n",buffer );
            strcpy(pathDesc, buffer);
            write_file(buffer);

            msg = "Filepath input.txt:";
            kirim(msg);
            //
            bzero(buffer, MAX);
            terima();
            printf("%s\n",buffer );
            strcpy(pathIn, buffer);
            write_file(buffer);


            msg = "Filepath output.txt:";
            kirim(msg);
            //
            bzero(buffer, MAX);
            terima();
            printf("%s\n",buffer );
            strcpy(pathOut, buffer);
            write_file(buffer);

            bzero(buffer, MAX);
            
        }else if(strcmp(buffer, "see") == 0){
            bzero(buffer, MAX);
            kirim(listProblem);
            bzero(buffer, MAX); 
        }
        
        else{
            msg = "\nPerintah tidak ditemukan, harap coba lagi";
            kirim(msg);
        }
        bzero(buffer, MAX);
    fclose(f_problems);
    }

    return 0;
}
