#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>
#define PORT 8080
#define MAX 1024

//global
int sock = 0;
char buffer[MAX] = {0};
char msg[MAX];
void* kirim2(void* arg){
    char* value = (char*)arg;
    send(sock , value , strlen(value) , 0);
}

void kirim(char string[]){
    pthread_t thread1;
    pthread_create(&thread1, NULL, &kirim2, (void* )string);
    pthread_join(thread1, NULL);
}

void* terima2(){
    read( sock , buffer, MAX);
}

void terima(){
    pthread_t thread1;
    pthread_create(&thread1, NULL, &terima2, NULL);
    pthread_join(thread1, NULL);
}

void regist(){
    terima();
    printf("%s\n",buffer );
    bzero(buffer, MAX);

    while(1){
        //get user name
        scanf("%s", msg);
        kirim(msg);

        terima();
        if(strcmp(buffer, "OK")== 0) {
            bzero(buffer, MAX);
            break;
        }else{
            printf("%s\n",buffer );
        }
        bzero(buffer, MAX);
    }

    terima();
    printf("%s\n",buffer );
    bzero(buffer, MAX);
    
    //get password
    while(1){
        int minHuruf=0, upcase=0, lowcase=0;
        scanf("%s", msg);
        if(strlen(msg) <6) {
            printf("password minimal 6 char length\ntry again\n");
            continue;
        }
        
        for(int i = 0; i<strlen(msg); i++){
            if(msg[i] >= 'A' && msg[i] <= 'Z' ){
                upcase = 1;
            }
            if(msg[i] >= 'a' && msg[i] <= 'z' ){
                lowcase = 1;
            }
        }
        if(upcase == 0 || lowcase == 0){
            printf("password should contain at least 1 uppercase and 1 lowercase\ntry again\n");
        }else{
            break;
        }
    }
    kirim(msg);
}

int login(){
    terima();
    printf("%s\n",buffer );
    bzero(buffer, MAX);

    while(1){
        terima();
        printf("%s\n",buffer );
        bzero(buffer, MAX);

        //get user name
        scanf("%s", msg);
        kirim(msg);
        //tambahin validasi

        terima();
        printf("%s\n",buffer );
        bzero(buffer, MAX);
        
        //get password
        scanf("%s", msg);
        kirim(msg);

        //get return statement success/fail
        terima();
        printf("%s\n",buffer );
        if(strcmp(buffer, "login success\n") == 0){
            bzero(buffer, MAX);
            break;
        }
        bzero(buffer, MAX);
    }
    return 1;
}

void send_file(FILE *fp)
{
    char data[MAX] = {0};

    while(fgets(data, MAX, fp)!=NULL)
    {
        strcat(buffer, data);
        printf("run\n");
    }
    kirim(buffer);
    bzero(buffer, MAX);
    return;
}


int main(int argc, char const *argv[]) {    

    struct sockaddr_in address;
    int valread;
    struct sockaddr_in serv_addr;
    int loged=0;


    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    //login/sign in system
    char flag[3];
    
    terima();
    printf("%s\n",buffer );
    bzero(buffer, MAX);
    while(1){
        scanf("%s", msg);
        if((strcmp(msg, "0") == 0) || (strcmp(msg, "1")==0)){
            kirim(msg);
            strcpy(flag, msg);
            break;
        }else{
            printf("\nInvalid Input, please try again\n");
        }
    }

    if(strcmp(flag, "0") == 0){
        regist();
        printf("registration success\nplease login with your new account\n");
        strcpy(flag, "1");
        loged = login();
    }else if(strcmp(flag, "1")==0){ //login
        loged = login();
        //get user name 
    }

    while (loged)
    {
        terima();
        printf("%s\n",buffer );
        bzero(buffer, MAX);

        scanf("%s", msg);
        kirim(msg);

        if(strcmp(msg, "add") == 0){

            while (1)
            {
                terima();
                printf("%s\n",buffer );
                bzero(buffer, MAX);
                
                scanf("%s", msg);
                kirim(msg);

                terima();
                if(strcmp(buffer, "OK")== 0) {
                    bzero(buffer, MAX);
                    break;
                }else{
                    printf("%s", buffer);
                    bzero(buffer, MAX);
                }
            }
            FILE *fp;

            terima();
            printf("%s\n",buffer );
            scanf("%s", msg);
            kirim(msg);
            char *filename = msg;
            fp = fopen(filename, "r");
            if(fp == NULL)
            {
                perror("[-]Error in reading file.");
                exit(1);
            }
            bzero(buffer, MAX);
            send_file(fp);
            fclose(fp);
            
            terima();
            printf("%s\n",buffer );
            bzero(buffer, MAX);
            scanf("%s", msg);
            kirim(msg);
            filename = msg;
            fp = fopen(filename, "r");
            if(fp == NULL)
            {
                perror("[-]Error in reading file.");
                exit(1);
            }
            send_file(fp);
            fclose(fp);


            terima();
            printf("%s\n",buffer );
            bzero(buffer, MAX);
            scanf("%s", msg);
            kirim(msg);
            filename = msg;
            fp = fopen(filename, "r");
            if(fp == NULL)
            {
                perror("[-]Error in reading file.");
                exit(1);
            }
            send_file(fp);
            fclose(fp);
            
        }else if(strcmp(msg, "see") == 0){
            terima();
            char * token = strtok(buffer, "\t");
            int i=0;
            while( token != NULL ) {
                i++;
                if(i % 2 != 2){
                    printf( "%s by ", token ); //printing each token
                }else{
                    printf( "%s\n", token ); //printing each token
                }
                token = strtok(NULL, "\t");

            }
            printf("\n");
            bzero(buffer, MAX);
            
        }
        else{
            terima();
            printf("%s\n",buffer );
            bzero(buffer, MAX);
        }

    }
    return 0;
}
