# Soal Shift Sistem Operasi Modul 3 Kelompok D04 2022

## Anggota Kelompok

<table>
    <tr>
        <th>NRP</th>
        <th>Nama</th>
    </tr>
    <tr>
        <td>Muhammad Lintang Panjerino</td>
        <td>5025201045</td>
    </tr>
    <tr>
        <td>Muhammad Yunus</td>
        <td>5025201171</td>
    </tr>
    <tr>
        <td>PEDRO T KORWA</td>
        <td>05111940007003</td>
    </tr>
<table>


# 1. Soal 1

## Deskripsi Soal
Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.
- Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.
- Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.
- Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.
- Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)
- Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

## Pembahasan dan Cara Pengerjaan
### 1a)

Pada soal 1a diperintahkan untuk download dua file zip (music.zip dan quote.zip) dari google drive, kemudian dua file tersebut di-unzip secara di dua folder berbeda (music dan quote) secara bersamaan menggunakan thread. Dibutuhkan dua fungsi, fungsi **dl_zip_file** dan **unzip_file** untuk menjalankannya. Pada fungsi dl_zip_file digunakan perintah *wget* untuk download file dari google drive, juga menggunakan thread. Kemudian fungsi unzip_file akan meng-unzip kedua file tadi. Pada kedua fungsi diterapkan **mutex** / mutual exclusion agar fungsi unzip menunggu fungsi dl_zip_file berhasil mendownload file-file terlebih dulu baru kemudian bisa diunzip

Berikut adalah fungsi dl_zip_file dan unzip_file :

```c
void* dl_zip_file(void *arg)
{
	mutex_status = 0;

	//music: https://drive.google.com/file/d/1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1/view?usp=sharing
	//quote: https://drive.google.com/file/d/1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt/view?usp=sharing

	char link_music[250] = "https://docs.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1";
	char link_quote[250] = "https://docs.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt";


	char *argv1[] = {"wget", "--no-check-certificate", link_music, "-O", "music.zip", NULL};
	char *argv2[] = {"wget", "--no-check-certificate", link_quote, "-O", "quote.zip", NULL};

        pthread_t id=pthread_self();

        if(pthread_equal(id,t_dl[0]))
        {
		int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/wget", argv1);
                } else {
                    while ((wait(&status)) > 0);
        	}
        }
        else if(pthread_equal(id,t_dl[1]))
        {
		int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/wget", argv2);
                } else {
                    while ((wait(&status)) > 0);
        	}
        }

	mutex_status = 1;

        return NULL;
}


// unzip music.zip dan quote.zip
void* unzip_file(void *arg)
{
	while(mutex_status != 1)
	{

	}

	char *argv1[] = {"unzip", "-q", "music.zip", "-d", "music/", NULL};
	char *argv2[] = {"unzip", "-q", "quote.zip", "-d", "quote/", NULL};

	pthread_t id=pthread_self();

        if(pthread_equal(id,t_unzip[0]))
        {
                int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/unzip", argv1);
                } else {
                    while ((wait(&status)) > 0);
        	}
        }
        else if(pthread_equal(id,t_unzip[1]))
        {
                int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/unzip", argv2);
                } else {
                    while ((wait(&status)) > 0);
        	}
        }

        mutex_status = 2;

	return NULL;
}
```

### 1b)

Program harus men-*decode* semua file .txt pada folder music dan quote dengan **base64**, lalu hasilnya dijadikan satu untuk masing-masing folder (music.txt dan quote.txt). Pengerjaannya harus bersamaan dengan menggunakan thread. Untuk menjalankannya dibuat fungsi **decode_file_base64**.

Berikut adalah fungsi decode_file_base64 :

```c
//decode base64
void* decode_file_base64(void *arg)
{
        while(mutex_status != 2)
        {

        }


	int link[2];
	pid_t pid;
	char foo[4096];

	DIR *dp;
	struct dirent *ep;

	pthread_t id=pthread_self();

	if(pthread_equal(id,t_decode[0]))
	{


		dp = opendir("/home/lintang/Lintang/Sistem_Operasi/music/");
		if(dp != NULL)
		{

			FILE *fptr1;
			fptr1 = fopen("/home/lintang/Lintang/Sistem_Operasi/music.txt", "a+");

	                while ((ep = readdir (dp)) != NULL) {
	                        if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0) {

					if (pipe(link)==-1)
						die("pipe");

					if ((pid = fork()) == -1)
						die("fork");


	                                char filename[150];
	                                strcpy(filename, "/home/lintang/Lintang/Sistem_Operasi/music/");
	                                strcat(filename, ep->d_name);

					if(pid == 0) {

						dup2 (link[1], STDOUT_FILENO);
						close(link[0]);
						close(link[1]);
						execl("/bin/base64", "base64", "-d", filename, (char *)0);
						die("execl");

					} else {

						close(link[1]);
						int nbytes = read(link[0], foo, sizeof(foo));
						fprintf(fptr1, "%.*s\n", nbytes, foo);
						wait(NULL);

					}

	                        }
	                }
	                (void) closedir (dp);
			fclose(fptr1);
	        }

	}

	else if(pthread_equal(id,t_decode[1]))
	{


		dp = opendir("/home/lintang/Lintang/Sistem_Operasi/quote/");
		if(dp != NULL)
		{

		        FILE *fptr2;
		        fptr2 = fopen("/home/lintang/Lintang/Sistem_Operasi/quote.txt", "a+");

	                while ((ep = readdir (dp)) != NULL) {
	                        if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0) {

					if (pipe(link)==-1) die("pipe");

					if ((pid = fork()) == -1) die("fork");


	                                char filename[150];
	                                strcpy(filename, "/home/lintang/Lintang/Sistem_Operasi/quote/");
	                                strcat(filename, ep->d_name);

					if(pid == 0) {

						dup2 (link[1], STDOUT_FILENO);
						close(link[0]);
						close(link[1]);
						execl("/bin/base64", "base64", "-d", filename, (char *)0);
						die("execl");

					} else {

						close(link[1]);
						int nbytes = read(link[0], foo, sizeof(foo));
						fprintf(fptr2, "%.*s\n", nbytes, foo);
						wait(NULL);

					}

	                        }
	                }
	                (void) closedir (dp);
			fclose(fptr2);
		}

	}

        mutex_status = 3;

	return NULL;
}
```

### 1c)

Setelah berhasil di-*decode*, file music.txt dan quote.txt dipindah ke folder baru bernama **hasil**. Untuk itu diperlukan dua fungsi, yaitu **make_hasil** untuk membuat folder bernama "hasil" dan **move_to_hasil** untuk memindahkan file music.txt dan quote.txt ke folder "hasil".

Berikut adalah fungsi make_hasil dan move_to_hasil :

```c
//make folder hasil
void* make_hasil(void *arg)
{
        while(mutex_status != 3)
        {

        }

        char *argv1[] = {"mkdir", "-p", "/home/lintang/Lintang/Sistem_Operasi/hasil/", NULL};

        pthread_t id=pthread_self();

        if(pthread_equal(id,t_make_folder[0]))
        {
                int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/mkdir", argv1);
                } else {
                    while ((wait(&status)) > 0);
                }
        }

        mutex_status = 4;

        return NULL;
}


//move file music.txt dan quote.txt
void* move_to_hasil(void *arg)
{
        while(mutex_status != 4)
        {

        }

        char *argv1[] = {"mv", "/home/lintang/Lintang/Sistem_Operasi/music.txt", "/home/lintang/Lintang/Sistem_Operasi/hasil/", NULL};
        char *argv2[] = {"mv", "/home/lintang/Lintang/Sistem_Operasi/quote.txt", "/home/lintang/Lintang/Sistem_Operasi/hasil/", NULL};

        pthread_t id=pthread_self();

        if(pthread_equal(id,t_move[0]))
        {
                int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/mv", argv1);
                } else {
                    while ((wait(&status)) > 0);
                }
        }
        else if(pthread_equal(id,t_move[1]))
        {
                int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/mv", argv2);
                } else {
                    while ((wait(&status)) > 0);
                }
        }

	mutex_status = 5;

	return NULL;
}
```

### 1d)

Setelah berhasil dipindahkan, folder **hasil** akan di-zip dengan password **mihinomenest[namauser]** yaitu **mihinomenestlintang** menggunakan fungsi zip_hasil.

Berikut adalah fungsi zip_hasil :

```c
//zip folder hasil
void* zip_hasil(void *arg)
{
        while(mutex_status != 5)
        {

        }

        char *argv1[] = {"zip", "-P", zip_password, "-r", "hasil.zip", "hasil/", NULL};

        pthread_t id=pthread_self();

        if(pthread_equal(id,t_zip_hasil[0]))
        {
                int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/zip", argv1);
                } else {
                    while ((wait(&status)) > 0);
                }
        }

        mutex_status = 6;

        return NULL;

}
```

### 1e)

Permintaan terakhir dari soal adalah program harus melakukan unzip file hasil.zip dan membuat file no.txt (dengan tulisan "No") secara bersamaan dengan menggunakan thread. Kemudian zip kedua file hasil dan file no.txt menjadi **hasil.zip**, dengan password yang sama seperti sebelumnya (mihinomenestlintang). Pada soal 1e ini dibutuhkan 3 fungsi, yaitu **unzip_hasil_create_no**, **move_file_no**, dan **last_zip**.

Berikut adalah fungsi unzip_hasil_create_no, move_file_no, dan last_zip :

```c
// unzip hasil.zip
void* unzip_hasil_create_no(void *arg)
{
        while(mutex_status != 6)
        {

        }

        char *argv1[] = {"unzip", "-P", zip_password, "-oq", "hasil.zip", "-d", "hasil", NULL};

        pthread_t id=pthread_self();

        if(pthread_equal(id,t_soal_e[0]))
        {
                int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/unzip", argv1);
                } else {
                    while ((wait(&status)) > 0);
                }
        }
        else if(pthread_equal(id,t_soal_e[1]))
        {
                int status;
                child = fork();
                if (child==0) {
                    FILE *fpointer;
		    fpointer = fopen("/home/lintang/Lintang/Sistem_Operasi/no.txt", "w+");

		    fprintf(fpointer, "%s\n", "No");
		    fclose(fpointer);
                } else {
                    while ((wait(&status)) > 0);
                }
        }

	mutex_status = 7;

	return NULL;
}


//move no.txt
void* move_file_no(void *arg)
{
        while(mutex_status != 7)
        {

        }

        char *argv1[] = {"mv", "/home/lintang/Lintang/Sistem_Operasi/no.txt", "/home/lintang/Lintang/Sistem_Operasi/hasil/", NULL};

        pthread_t id=pthread_self();

        if(pthread_equal(id,t_move_no[0]))
        {
                int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/mv", argv1);
                } else {
                    while ((wait(&status)) > 0);
                }
        }

	mutex_status = 8;

	return NULL;
}


//zip hasil dan no.txt
void* last_zip(void *arg)
{
        while(mutex_status != 8)
        {

        }

        char *argv1[] = {"zip", "-P", zip_password, "hasil.zip", "hasil/no.txt", "hasil/music.txt", "hasil/quote.txt", NULL};

        pthread_t id=pthread_self();

        if(pthread_equal(id,t_last_zip[0]))
        {
                int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/zip", argv1);
                } else {
                    while ((wait(&status)) > 0);
                }
        }

        mutex_status = 9;

        return NULL;
}
```

## Kendala

- Pada awalnya bingung bagaimana cara menjalankan banyak thread, bagaimana agar suatu pekerjaan(fungsi) dapat diselesaikan dulu baru pekerjaan(fungsi) lain dijalankan. Akhirnya menemukan solusi di modul yaitu dengan menggunakan **mutex** / mutual exclusion dengan cara mendeklarasikan variabel global ```int mutex_status;```, kemudian pada setiap fungsi di-set mutex_status sesuai urutannya.
- Sempat bermasalah dengan decode base64 karena tidak bisa menangkap hasil decode-nya dari fungsi exec(). Setelah itu ditemukan cara di stackoverflow untuk menangkap hasil decode base64 dari fungsi exec() yaitu dengan menggunakan kombinasi **dup2()**, **close()**, **read()**, dan **wait**.

## 2. Soal 2

#### Deskripsi Soal:
1.  Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:
### 2.A

Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file **users.txt** dengan format **username:password.** Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di **users.txt** yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:
    

-   Username unique (tidak boleh ada user yang memiliki username yang sama)
    
-   Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil
    

Format users.txt:

  

**users.txt**

username:password

username2:password2
#### Pembahasan
Karen soal meminta untuk membuat komnikasi antara client  dan server, maka dalam hal ini diperlukan socket programming dengan mengunakan socket dengan kode dasar untuk menghubungnkan server dan client seperti berikut:

server.c
```c
//define 
#define PORT 8080
#define MAX 1024

//global
char buffer[MAX] = {0};
char user[MAX] = {0};
char absPath[300] = "/home/yunus/Praktikum/modul3/Server";
int new_socket;
char *msg;

//global file
FILE* f_akun;
FILE* f_problems;
char akunPath[300];
char problemsPath[300];
char dirPath[300];

int  main(int  argc, char  const *argv[]) {
	int  server_fd;
	struct  sockaddr_in  address;
	int  opt = 1;
	int  addrlen = sizeof(address);

	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
		perror("socket failed");
		exit(EXIT_FAILURE);
	}

	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons( PORT );

	if (bind(server_fd, (struct  sockaddr *)&address, 
	sizeof(address))<0) {
		perror("bind failed");
		exit(EXIT_FAILURE);
	}

	if (listen(server_fd, 3) < 0) {
		perror("listen");
		exit(EXIT_FAILURE);
	}

	if ((new_socket = accept(server_fd, (struct  sockaddr *)&address, (socklen_t*)&addrlen))<0) {
		perror("accept");
		exit(EXIT_FAILURE);
	}
}
````

client.c
```c
//define
#define PORT 8080
#define MAX 1024

//global
int sock = 0;
char buffer[MAX] = {0};
char msg[MAX];

int  main(int  argc, char  const *argv[]) {
	struct  sockaddr_in  address;
	struct  sockaddr_in  serv_addr;

	int  loged=0;

	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("\n Socket creation error \n");
	return -1;
	}
	memset(&serv_addr, '0', sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);

	if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
		printf("\nInvalid address/ Address not supported \n");
		return -1;
	}
	if (connect(sock, (struct  sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
		printf("\nConnection Failed \n");
		return -1;
	}
}
```
Dalam mengirm dan menerima pesan dalam antara server dan client saya mengunakan suatu fungsi utilitas yang akan digunakan untuk mengirim dan menerima pesan dengan mengunkan thread. Hal ini untuk memastikan pesan masuk dan keluar dari server dan client tidak saling mendahului/bersama (conccurrent). Maka thread dengan memanfaatkan fungsi join digunakan. Dengan implementasi seperti berikut:
```c
//global
int sock = 0;
char buffer[MAX] = {0};
char msg[MAX];
void* kirim2(void* arg){
    char* value = (char*)arg;
    send(sock , value , strlen(value) , 0);
}

void kirim(char string[]){
    pthread_t thread1;
    pthread_create(&thread1, NULL, &kirim2, (void* )string);
    pthread_join(thread1, NULL);
}

void* terima2(){
    read( sock , buffer, MAX);
}

void terima(){
    pthread_t thread1;
    pthread_create(&thread1, NULL, &terima2, NULL);
    pthread_join(thread1, NULL);
}
```

2a.1 
saat pertama server dan client telah terhubung client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file **users.txt** dengan format **username:password.**
Pada proses ini, jika client memilih registrasi maka server akan menanyakan username dan password yang akan digunakan client.
- server.c
```c
msg = "Enter 0 for Register \nEnter 1 for login ";
    kirim(msg);
    
    bzero(buffer, MAX);
    terima();
    printf("%s\n",buffer );
    
    if(strcmp(buffer, "0") == 0){
        regist();
        isAuth = login();
    }else if (strcmp(buffer, "1") == 0){ //login
        isAuth = login();
    }
    bzero(buffer, MAX);
```
- client.c
```c
    char flag[3];
    
    terima();
    printf("%s\n",buffer );
    bzero(buffer, MAX);
    while(1){
        scanf("%s", msg);
        if((strcmp(msg, "0") == 0) || (strcmp(msg, "1")==0)){
            kirim(msg);
            strcpy(flag, msg);
            break;
        }else{
            printf("\nInvalid Input, please try again\n");
        }
    }

    if(strcmp(flag, "0") == 0){
        regist();
        printf("registration success\nplease login with your new account\n");
        strcpy(flag, "1");
        loged = login();
    }else if(strcmp(flag, "1")==0){ //login
        loged = login();
        //get user name 
    }

```
Paa tahap ini server akan mengirim pesan untuk meminta user memilih untuk register atau login. jika memilih register maka fungsi register akan dijalankan begitupun sebaliknya
<br>
<br>

#### Register
- server.c
```c
void regist(){
    char akun[MAX];   
        f_akun = fopen(akunPath, "a+");
        //get username
        char buf[MAX];
        char tempString[MAX];
        while((fgets(buf, MAX, f_akun)!=NULL)) {  //good to handle error as well
            strcat(tempString, buf);
            strcat(tempString, "\n");
        }

        msg = "Please fill this registraton form: \nUsername: ";
        kirim(msg);
        while (1)
        {            
            bzero(buffer, MAX);
            terima();
            printf("%s\n",buffer ); 
            if(strstr(tempString, buffer)==NULL) {
                msg = "OK";
                kirim(msg);
                break;
            }else{
                msg = "Username is exist, please try different username";
                kirim(msg);
            }
            /* code */
        }
        
        //keep username
        strcpy(akun, buffer);
        strcat(akun, ":");

        //get pass
        msg = "\nPassword: ";
        kirim(msg);

        bzero(buffer, MAX);
        terima();
        printf("%s\n",buffer);
        
        //keep pass
        strcat(akun, buffer);
        strcat(akun, "\n");

        fputs(akun, f_akun);
        fclose(f_akun);
}


```


-  client.c
```c
void regist(){
    terima();
    printf("%s\n",buffer );
    bzero(buffer, MAX);

    while(1){
        //get user name
        scanf("%s", msg);
        kirim(msg);

        terima();
        if(strcmp(buffer, "OK")== 0) {
            bzero(buffer, MAX);
            break;
        }else{
            printf("%s\n",buffer );
        }
        bzero(buffer, MAX);
    }

    terima();
    printf("%s\n",buffer );
    bzero(buffer, MAX);
    
    //get password
    while(1){
        int minHuruf=0, upcase=0, lowcase=0;
        scanf("%s", msg);
        if(strlen(msg) <6) {
            printf("password minimal 6 char length\ntry again\n");
            continue;
        }
        
        for(int i = 0; i<strlen(msg); i++){
            if(msg[i] >= 'A' && msg[i] <= 'Z' ){
                upcase = 1;
            }
            if(msg[i] >= 'a' && msg[i] <= 'z' ){
                lowcase = 1;
            }
        }
        if(upcase == 0 || lowcase == 0){
            printf("password should contain at least 1 uppercase and 1 lowercase\ntry again\n");
        }else{
            break;
        }
    }
    kirim(msg);
}
```
Server akan membuat file usert.txt dengan path yang sudah didefinisikan sebelumnya pada global. Selanjutnya server akan meminta untuk user memasukkan username dan password yang ingin digunakan. Setelah username dan password diterima. Pada client sida akan dilakukan validasi password, yaitu password harus terdiri dari mininmal 6 character dan minimal 1 huruf besar dan kecil. setelahnya server akan melakukan pengecekkan apakah username tersebut telah ada pad file user.txt apa belum. jika belum akan dilanjutkan. Jika sudah maka client akan kembali diminta memasukan username dan password yang lain hingga input tersebut valid. Jika telah valid user name dan password akan disimpan pada user.txt dengan format sesuai soal `username:password`. Selanjutnya user akan diarakahkan pada sesi login.
### login
- server.c 
```c
int login(){
        char akun[MAX];
        f_akun = fopen(akunPath, "r");
        int success = 0;

        msg = "Login\n";
        kirim(msg);

        char buf[MAX];
        char tempString[MAX];
        while((fgets(buf, MAX, f_akun)!=NULL)) {  //good to handle error as well
            strcat(tempString, buf);
            strcat(tempString, "\n");
        }
       while (success == 0)
       {    
            msg = "username login: ";
            kirim(msg);
            
            bzero(buffer, MAX);
            terima();
            printf("%s\n",buffer );

            //save the curr-username
            strcpy(user, buffer);

            //keep username
            strcpy(akun, buffer);
            strcat(akun, ":");

            //get pass
            msg = "\nPassword: ";
            kirim(msg);

            bzero(buffer, MAX);
            terima();
            printf("%s\n",buffer);
            
            //keep pass
            strcat(akun, buffer);

            if(strstr(tempString, akun)!=NULL) {
                success = 1;
            }
            printf("%d\n",success);
            if(success == 0){
                msg = "!! wrong username/password\n";
                kirim(msg);
            }
            else{
                msg = "login success\n";
                kirim(msg);
            }    
            bzero(akun, MAX);
        }
        fclose(f_akun); 
    return success;
}
```

- client.c
```c
int login(){
    terima();
    printf("%s\n",buffer );
    bzero(buffer, MAX);

    while(1){
        terima();
        printf("%s\n",buffer );
        bzero(buffer, MAX);

        //get user name
        scanf("%s", msg);
        kirim(msg);
        //tambahin validasi

        terima();
        printf("%s\n",buffer );
        bzero(buffer, MAX);
        
        //get password
        scanf("%s", msg);
        kirim(msg);

        //get return statement success/fail
        terima();
        printf("%s\n",buffer );
        if(strcmp(buffer, "login success\n") == 0){
            bzero(buffer, MAX);
            break;
        }
        bzero(buffer, MAX);
    }
    return 1;
}
```
server akan meminta client untuk memasukan username dan password mereka, selanjut dilakukan pengecekkan apaka username  dan password ada, jika status isAuth akan menjadi 1 jika tidak user akan diminta untuk memasukkan username dan password yang valid.

### 2.B
Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama **problems.tsv** yang terdiri dari **judul problem dan author problem (berupa username dari author), yang dipisah dengan \t.** File otomatis dibuat saat server dijalankan.
  
#### Pembahasan
Pada server kita hanya perlu membuat file baru dengan mengunakn fungsi fopen dengan perintah "a+" sama seperti saat membuat user.txt, pertintah "a+" akan membuat file jika file belum dibuat, jika sudah isinya akan dapat dibaca dan di write dalah hal ini adalah append ke file tersebut.
- server.c
```c
int main(){
	//makeprblem.tsv
	strcpy(problemsPath, absPath);
	strcat(problemsPath, "/problems.tsv");
	f_problems = fopen(problemsPath, "a+");
	fclose(f_problems);
}
```
hal tersebut dilakukan diawal saat pertama kali server dijalankan.


### 2.C
**Client yang telah login**, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:
    

-   Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
    
-   Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
    
-   Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
    
-   Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)
    

Contoh:

- Client-side

add

- Server-side

Judul problem:

Filepath description.txt:

Filepath input.txt:

Filepath output.txt:

  

- Client-side

<judul-problem-1>

<Client/description.txt-1>

<Client/input.txt-1>

<Client/output.txt-1>

  

Seluruh file akan disimpan oleh server ke dalam folder dengan nama <judul-problem> yang di dalamnya terdapat file description.txt, input.txt dan output.txt. Penambahan problem oleh client juga akan mempengaruhi file **problems.tsv.**

#### Pembahasan


- server.c 
```c
while (isAuth)
    {
        msg = "welcome, anda telah login\nketik 'add' untuk menambahkan problem/soal baru\nketik 'see' untuk melihat list problem\nketik 'download <judul-problem>' untuk mendownload problem\nketik 'submit <judul-problem> <path-output.txt>' untuk submit jawaban" ;
        kirim(msg);

        terima();
        printf("%s\n",buffer);
```
Pertama server mengecek apakah client sudah login atau (isAuth = 1), jika sudah selanjutnya server akan menampilkan pilihan command/opsi yang dapat dilih oleh client, selain command tersebut, maka perintah dianggap tidak valid. command-command yang dapat dilakukan sesuai dengan permintaan soal. 

- client.c
```c
while (loged)
    {
        terima();
        printf("%s\n",buffer );
        bzero(buffer, MAX);

        scanf("%s", msg);
        kirim(msg);
```
Selanjutnya client memasukan pilihan command yang akan digunakan dalam hal ini adalah add.

### add
Dalam melakukan command add saya mengunakan fungsi utilitas untuk mengirim data file dari client, dan menerima data file di server
- client.c
```c
void send_file(FILE *fp)
{
    char data[MAX] = {0};

    while(fgets(data, MAX, fp)!=NULL)
    {
        strcat(buffer, data);
        printf("run\n");
    }
    kirim(buffer);
    bzero(buffer, MAX);
    return;
}
```
- server.c 
```c

void write_file(char fileName[])
{
    int n; 
    FILE *fp;
    char *filename = fileName;

    char filePath[MAX];
    strcpy(filePath, dirPath);
    strcat(filePath, "/");
    strcat(filePath, fileName);

    fp = fopen(filePath, "a+");
    if(fp==NULL)
    {
        perror("[-]Error in creating file.");
        exit(1);
    }
    bzero(buffer, MAX);
    terima();
    fprintf(fp, "%s", buffer);
    printf("%s",buffer);
    bzero(buffer, MAX);
    fclose(fp);
    return;
}
```
Kedua fungsi utilitas tersebut digunakan untuk mengirim file dari client ke server.

- server.c
```c
f_problems = fopen(problemsPath, "a+");
char  listProblem[MAX]; //semua problem yang ada
char  buf[MAX];
while((fgets(buf, MAX, f_problems)!=NULL)) { 
	strcat(listProblem, buf);
}
  if(strcmp(buffer, "add") == 0){
       char saveData[MAX];
       //check judul if exist
       while (1)
       {
           msg = "Judul Problem";
           kirim(msg);
                           
           bzero(buffer, MAX);
           terima();
           printf("%s\n",buffer );

           if(strstr(listProblem, buffer)==NULL) {
               msg = "OK";
               kirim(msg);
               break;
          }else{
               msg = "Judul telah ada, silahkan gunakn judul lain\n";
               kirim(msg);                    
           }
       }
       //here buffer = judul
       mkdir(buffer, 0777);
       strcpy(dirPath, absPath);
       strcat(dirPath, "/");
       strcat(dirPath, buffer);
       //update database problems.tsv
       strcpy(saveData, buffer);
       strcat(saveData, "\t");
       strcat(saveData, user);

       fprintf(f_problems, "%s\n", saveData);

       char pathDesc[MAX];
       char pathIn[MAX];
       char pathOut[MAX];

       msg = "Filepath description.txt:";
       kirim(msg);
       //
       bzero(buffer, MAX);
       terima();
       printf("%s\n",buffer );
       strcpy(pathDesc, buffer);
       write_file(buffer);

       msg = "Filepath input.txt:";
       kirim(msg);
       //
       bzero(buffer, MAX);
       terima();
       printf("%s\n",buffer );
       strcpy(pathIn, buffer);
       write_file(buffer);

       msg = "Filepath output.txt:";
       kirim(msg);
       //
       bzero(buffer, MAX);
       terima();
       printf("%s\n",buffer );
       strcpy(pathOut, buffer);
       write_file(buffer);
       bzero(buffer, MAX);
   }
```


sdfsdf
- client.c
```c
   if(strcmp(msg, "add") == 0){
         while (1)
         {
             terima();
             printf("%s\n",buffer );
             bzero(buffer, MAX);
             
             scanf("%s", msg);
             kirim(msg);

             terima();
             if(strcmp(buffer, "OK")== 0) {
                 bzero(buffer, MAX);
                 break;
             }else{
                 printf("%s", buffer);
                 bzero(buffer, MAX);
             }
         }
         FILE *fp;

         terima();
         printf("%s\n",buffer );
         scanf("%s", msg);
         kirim(msg);
         char *filename = msg;
         fp = fopen(filename, "r");
         if(fp == NULL)
         {
             perror("[-]Error in reading file.");
             exit(1);
         }
         bzero(buffer, MAX);
         send_file(fp);
         fclose(fp);
         
         terima();
         printf("%s\n",buffer );
         bzero(buffer, MAX);
         scanf("%s", msg);
         kirim(msg);
         filename = msg;
         fp = fopen(filename, "r");
         if(fp == NULL)
         {
             perror("[-]Error in reading file.");
             exit(1);
         }
         send_file(fp);
         fclose(fp);


         terima();
         printf("%s\n",buffer );
         bzero(buffer, MAX);
         scanf("%s", msg);
         kirim(msg);
         filename = msg;
         fp = fopen(filename, "r");
         if(fp == NULL)
         {
             perror("[-]Error in reading file.");
             exit(1);
         }
         send_file(fp);
         fclose(fp);
     }
```
- Server akan meminta client memasukkan judul problem yang diinginkan
- Judul yang dimasukkan client akan dicek terlebih dahulu apakah sudah ada di dalam `listProblems` jika ada maka client akan diminta memasukkan judul lain, jika belum akan dilanjutkan pada input selanjutnya\
- Jika judul telah valid, server akan otomatis membuatkan folder baru di server-side dengan nama folder merupakan judul yang dimasukkan tersebut. nantinya file-file kebutuhan problem akan disatukan pada folder tersebut.
- Server juga akan melakukan write judul probelm ke problem.tsv (`f_problems`) dan authornya dipisahkan denga tab `\t`.
- Server meminta client memasukkan nama file yang sekaligus merupakan file path description.txt yang berisikan descripsi dari problem yang akan dimasukkan
- Client memasukkan nama/file path description.txt dan akan memanggil fungsi utilitas `send_file(filePointer)` yang akan membaca file yang yang dimaksudkan user.
- File akan dibaca sampai habis lalu isinya disimpan pada variable buffer, setelah kesulurahan file telah dibaca dan disimpan pada buffer, selanjutnya buffer akan dikirim ke server.
- Setelah server mendapatkan nama file yang diinginkan client. server akan merangkai filepath ke folder judul problem dan memasukkan nama file tersebut. Sehingga file berhasil dibuat didalam folder judul problem
- selanjutnya server akan memanggil fungsi utilitas `write_file(namaFile)` fungsi utilitas akan menanggkap semua isi file yang telah dikirimkan client dari fungsi `send_file(fp)` sebelumnya.
- setelah isi file berhasil diterima di server-side, server akan melakukan write ke file yang dibuat seblumnya dengan isi dari file yang dikirim user tersebut. Sehingga server telah berhasil mendapatkan dan membuat salinan file dari file yang dikirim user tersebut.
- Langkah untuk description.txt akan diulang untuk dile input.txt dan output.txt, sehingga pada folder judul problem semua file yang dibutuhkan telah berhasil dibuat.
- Problem pun berhasil ditambahkan dan command "add" telah selesai. Client kembali diarahkan untuk memasukkan command selanjutnya


### 2.D
  **Client yang telah login**, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). Format yang akan ditampilkan oleh server adalah sebagai berikut:  

judul-problem-1 by author1

judul-problem-2 by  author2

#### Pembahasan
- client akan diminta memasukan perintah yang teredia, dalam hal ini client memasukkan command "see"

- server.c
```c
f_problems = fopen(problemsPath, "a+");
char  listProblem[MAX]; //semua problem yang ada
char  buf[MAX];
while((fgets(buf, MAX, f_problems)!=NULL)) { 
	strcat(listProblem, buf);
}
else if(strcmp(buffer, "see") == 0){
            bzero(buffer, MAX);
            kirim(listProblem);
            bzero(buffer, MAX); 
        }
```
server yang telah menyimipan semua data pada problem.tsv pada string `listProblems` yang berisi semua judul problem dan authornya dipisahkan `\t` hanya tinggal mengirimkan string tersebut ke client


- client.c
```c
else if(strcmp(msg, "see") == 0){
            terima();
            char * token = strtok(buffer, "\t");
            int i=0;
            while( token != NULL ) {
                i++;
                if(i % 2 != 2){
                    printf( "%s by ", token ); //printing each token
                }else{
                    printf( "%s\n", token ); //printing each token
                }
                token = strtok(NULL, "\t");

            }
            printf("\n");
            bzero(buffer, MAX);
            
        }
```
- client akan mendapatkan semua list problem tersebut dan melakukan penyesuaian output karena output yang diinginkan untuk dicetak adalah dengan format "judul by author"
- String tadi akan dipisah dengan mengunakan `strtok()` dengan delimeter berupa tab `\t` jadi token akan berisi setiap kata/kalimat yang dipisahkan dengan tab dalam string.
- selanjutnya menyersuaikan output jika pada iterasi ganjil (kata untuk 'judul') akan ditambhakan 'by"
- untuk iterasi ganjil (untuk kata 'author') akan diakhiri dengan endline
- dicetak hingga semua isi string (problems0 telah diprint.
- List problem berhasil ditampilkan, setelahnya client akan dikembalikan ke menu untuk memasukkan command.
### Kendala

- Kesulitan dalam memahami thread
- Sering terjadi tabrakan antara pesan masukan dan kluar dari server/client jika tanpa thread_join
- Kesulitan dalam debugging karena harus memulai program dari awal
- point soal cukup banyak

