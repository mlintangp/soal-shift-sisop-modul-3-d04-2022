#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <wait.h>
#include <dirent.h>

#define die(e) do { fprintf(stderr, "%s\n", e); exit(EXIT_FAILURE); } while (0);
#define zip_password "mihinomenestlintang"

pthread_t t_dl[2], t_unzip[2], t_decode[2], t_make_folder[1], t_move[2], t_zip_hasil[1], t_soal_e[2], t_move_no[1], t_last_zip[1];
pid_t child;
int mutex_status;


// download music.zip dan quote.zip
void* dl_zip_file(void *arg)
{
	mutex_status = 0;

	//music: https://drive.google.com/file/d/1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1/view?usp=sharing
	//quote: https://drive.google.com/file/d/1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt/view?usp=sharing

	char link_music[250] = "https://docs.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1";
	char link_quote[250] = "https://docs.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt";


	char *argv1[] = {"wget", "--no-check-certificate", link_music, "-O", "music.zip", NULL};
	char *argv2[] = {"wget", "--no-check-certificate", link_quote, "-O", "quote.zip", NULL};

        pthread_t id=pthread_self();

        if(pthread_equal(id,t_dl[0]))
        {
		int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/wget", argv1);
                } else {
                    while ((wait(&status)) > 0);
        	}
        }
        else if(pthread_equal(id,t_dl[1]))
        {
		int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/wget", argv2);
                } else {
                    while ((wait(&status)) > 0);
        	}
        }

	mutex_status = 1;

        return NULL;
}


// unzip music.zip dan quote.zip
void* unzip_file(void *arg)
{
	while(mutex_status != 1)
	{

	}

	char *argv1[] = {"unzip", "-q", "music.zip", "-d", "music/", NULL};
	char *argv2[] = {"unzip", "-q", "quote.zip", "-d", "quote/", NULL};

	pthread_t id=pthread_self();

        if(pthread_equal(id,t_unzip[0]))
        {
                int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/unzip", argv1);
                } else {
                    while ((wait(&status)) > 0);
        	}
        }
        else if(pthread_equal(id,t_unzip[1]))
        {
                int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/unzip", argv2);
                } else {
                    while ((wait(&status)) > 0);
        	}
        }

        mutex_status = 2;

	return NULL;
}


//decode base64
void* decode_file_base64(void *arg)
{
        while(mutex_status != 2)
        {

        }


	int link[2];
	pid_t pid;
	char foo[4096];

	DIR *dp;
	struct dirent *ep;

	pthread_t id=pthread_self();

	if(pthread_equal(id,t_decode[0]))
	{


		dp = opendir("/home/lintang/Lintang/Sistem_Operasi/music/");
		if(dp != NULL)
		{

			FILE *fptr1;
			fptr1 = fopen("/home/lintang/Lintang/Sistem_Operasi/music.txt", "a+");

	                while ((ep = readdir (dp)) != NULL) {
	                        if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0) {

					if (pipe(link)==-1)
						die("pipe");

					if ((pid = fork()) == -1)
						die("fork");


	                                char filename[150];
	                                strcpy(filename, "/home/lintang/Lintang/Sistem_Operasi/music/");
	                                strcat(filename, ep->d_name);

					if(pid == 0) {

						dup2 (link[1], STDOUT_FILENO);
						close(link[0]);
						close(link[1]);
						execl("/bin/base64", "base64", "-d", filename, (char *)0);
						die("execl");

					} else {

						close(link[1]);
						int nbytes = read(link[0], foo, sizeof(foo));
						fprintf(fptr1, "%.*s\n", nbytes, foo);
						wait(NULL);

					}

	                        }
	                }
	                (void) closedir (dp);
			fclose(fptr1);
	        }

	}

	else if(pthread_equal(id,t_decode[1]))
	{


		dp = opendir("/home/lintang/Lintang/Sistem_Operasi/quote/");
		if(dp != NULL)
		{

		        FILE *fptr2;
		        fptr2 = fopen("/home/lintang/Lintang/Sistem_Operasi/quote.txt", "a+");

	                while ((ep = readdir (dp)) != NULL) {
	                        if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0) {

					if (pipe(link)==-1) die("pipe");

					if ((pid = fork()) == -1) die("fork");


	                                char filename[150];
	                                strcpy(filename, "/home/lintang/Lintang/Sistem_Operasi/quote/");
	                                strcat(filename, ep->d_name);

					if(pid == 0) {

						dup2 (link[1], STDOUT_FILENO);
						close(link[0]);
						close(link[1]);
						execl("/bin/base64", "base64", "-d", filename, (char *)0);
						die("execl");

					} else {

						close(link[1]);
						int nbytes = read(link[0], foo, sizeof(foo));
						fprintf(fptr2, "%.*s\n", nbytes, foo);
						wait(NULL);

					}

	                        }
	                }
	                (void) closedir (dp);
			fclose(fptr2);
		}

	}

        mutex_status = 3;

	return NULL;
}


//make folder hasil
void* make_hasil(void *arg)
{
        while(mutex_status != 3)
        {

        }

        char *argv1[] = {"mkdir", "-p", "/home/lintang/Lintang/Sistem_Operasi/hasil/", NULL};

        pthread_t id=pthread_self();

        if(pthread_equal(id,t_make_folder[0]))
        {
                int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/mkdir", argv1);
                } else {
                    while ((wait(&status)) > 0);
                }
        }

        mutex_status = 4;

        return NULL;
}


//move file music.txt dan quote.txt
void* move_to_hasil(void *arg)
{
        while(mutex_status != 4)
        {

        }

        char *argv1[] = {"mv", "/home/lintang/Lintang/Sistem_Operasi/music.txt", "/home/lintang/Lintang/Sistem_Operasi/hasil/", NULL};
        char *argv2[] = {"mv", "/home/lintang/Lintang/Sistem_Operasi/quote.txt", "/home/lintang/Lintang/Sistem_Operasi/hasil/", NULL};

        pthread_t id=pthread_self();

        if(pthread_equal(id,t_move[0]))
        {
                int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/mv", argv1);
                } else {
                    while ((wait(&status)) > 0);
                }
        }
        else if(pthread_equal(id,t_move[1]))
        {
                int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/mv", argv2);
                } else {
                    while ((wait(&status)) > 0);
                }
        }

	mutex_status = 5;

	return NULL;
}


//zip folder hasil
void* zip_hasil(void *arg)
{
        while(mutex_status != 5)
        {

        }

        char *argv1[] = {"zip", "-P", zip_password, "-r", "hasil.zip", "hasil/", NULL};

        pthread_t id=pthread_self();

        if(pthread_equal(id,t_zip_hasil[0]))
        {
                int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/zip", argv1);
                } else {
                    while ((wait(&status)) > 0);
                }
        }

        mutex_status = 6;

        return NULL;

}


// unzip hasil.zip
void* unzip_hasil_create_no(void *arg)
{
        while(mutex_status != 6)
        {

        }

        char *argv1[] = {"unzip", "-P", zip_password, "-oq", "hasil.zip", "-d", "hasil", NULL};

        pthread_t id=pthread_self();

        if(pthread_equal(id,t_soal_e[0]))
        {
                int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/unzip", argv1);
                } else {
                    while ((wait(&status)) > 0);
                }
        }
        else if(pthread_equal(id,t_soal_e[1]))
        {
                int status;
                child = fork();
                if (child==0) {
                    FILE *fpointer;
		    fpointer = fopen("/home/lintang/Lintang/Sistem_Operasi/no.txt", "w+");

		    fprintf(fpointer, "%s\n", "No");
		    fclose(fpointer);
                } else {
                    while ((wait(&status)) > 0);
                }
        }

	mutex_status = 7;

	return NULL;
}


//move no.txt
void* move_file_no(void *arg)
{
        while(mutex_status != 7)
        {

        }

        char *argv1[] = {"mv", "/home/lintang/Lintang/Sistem_Operasi/no.txt", "/home/lintang/Lintang/Sistem_Operasi/hasil/", NULL};

        pthread_t id=pthread_self();

        if(pthread_equal(id,t_move_no[0]))
        {
                int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/mv", argv1);
                } else {
                    while ((wait(&status)) > 0);
                }
        }

	mutex_status = 8;

	return NULL;
}


//zip hasil dan no.txt
void* last_zip(void *arg)
{
        while(mutex_status != 8)
        {

        }

        char *argv1[] = {"zip", "-P", zip_password, "hasil.zip", "hasil/no.txt", "hasil/music.txt", "hasil/quote.txt", NULL};

        pthread_t id=pthread_self();

        if(pthread_equal(id,t_last_zip[0]))
        {
                int status;
                child = fork();
                if (child==0) {
                    execv("/usr/bin/zip", argv1);
                } else {
                    while ((wait(&status)) > 0);
                }
        }

        mutex_status = 9;

        return NULL;
}


int main(void)
{
        int i=0;
        int err;

        while(i<2)
        {
                err=pthread_create(&(t_dl[i]),NULL,&dl_zip_file,NULL);
                if(err!=0)
                {
                        printf("\n can't create thread : [%s]",strerror(err));
                }
                else
                {
//                        printf("\n create thread success\n");
                }
                i++;
        }

	i=0;
        while(i<2)
        {
                err=pthread_create(&(t_unzip[i]),NULL,&unzip_file,NULL);
                if(err!=0)
                {
                        printf("\n can't create thread : [%s]",strerror(err));
                }
                else
                {
//                        printf("\n create thread success\n");
                }
                i++;
        }

	pthread_join(t_dl[0],NULL);
        pthread_join(t_dl[1],NULL);

        pthread_join(t_unzip[0],NULL);
        pthread_join(t_unzip[1],NULL);

	/*decode*/
        i=0;
        while(i<2)
        {
                err=pthread_create(&(t_decode[i]),NULL,&decode_file_base64,NULL);
                if(err!=0)
                {
                        printf("\n can't create thread : [%s]",strerror(err));
                }
                else
                {
//                        printf("\n create thread success\n");
                }
                i++;
        }

        pthread_join(t_decode[0],NULL);
        pthread_join(t_decode[1],NULL);

	/*mkdir hasil*/
        i=0;
        while(i<1)
        {
                err=pthread_create(&(t_make_folder[i]),NULL,&make_hasil,NULL);
                if(err!=0)
                {
                        printf("\n can't create thread : [%s]",strerror(err));
                }
                else
                {
//                        printf("\n create thread success\n");
                }
                i++;
        }

        pthread_join(t_make_folder[0],NULL);

	/*move to hasil*/
        i=0;
        while(i<2)
        {
                err=pthread_create(&(t_move[i]),NULL,&move_to_hasil,NULL);
                if(err!=0)
                {
                        printf("\n can't create thread : [%s]",strerror(err));
                }
                else
                {
//                        printf("\n create thread success\n");
                }
                i++;
        }

        pthread_join(t_move[0],NULL);
        pthread_join(t_move[1],NULL);

	/*zip hasil*/
        i=0;
        while(i<1)
        {
                err=pthread_create(&(t_zip_hasil[i]),NULL,&zip_hasil,NULL);
                if(err!=0)
                {
                        printf("\n can't create thread : [%s]",strerror(err));
                }
                else
                {
//                        printf("\n create thread success\n");
                }
                i++;
        }

        pthread_join(t_zip_hasil[0],NULL);

	/*unzip + create no*/
        i=0;
        while(i<2)
        {
                err=pthread_create(&(t_soal_e[i]),NULL,&unzip_hasil_create_no,NULL);
                if(err!=0)
                {
                        printf("\n can't create thread : [%s]",strerror(err));
                }
                else
                {
//                        printf("\n create thread success\n");
                }
                i++;
        }

        pthread_join(t_soal_e[0],NULL);
        pthread_join(t_soal_e[1],NULL);

	/*move file no*/
        i=0;
        while(i<1)
        {
                err=pthread_create(&(t_move_no[i]),NULL,&move_file_no,NULL);
                if(err!=0)
                {
                        printf("\n can't create thread : [%s]",strerror(err));
                }
                else
                {
//                        printf("\n create thread success\n");
                }
                i++;
        }

        pthread_join(t_move_no[0],NULL);

	/*zip updated hasil*/
        i=0;
        while(i<1)
        {
                err=pthread_create(&(t_last_zip[i]),NULL,&last_zip,NULL);
                if(err!=0)
                {
                        printf("\n can't create thread : [%s]",strerror(err));
                }
                else
                {
//                        printf("\n create thread success\n");
                }
                i++;
        }

        pthread_join(t_last_zip[0],NULL);


        exit(0);
        return 0;
}
